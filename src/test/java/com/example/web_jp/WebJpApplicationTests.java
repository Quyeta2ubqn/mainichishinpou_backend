package com.example.web_jp;

import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;


@SpringBootTest
class WebJpApplicationTests {

	@Test
	void testSum(){
		Assert.assertEquals(3, Integer.sum(1,2));
	}

}
