package com.example.web_jp.config;

import com.example.web_jp.common.security.JwtProvider;
import com.example.web_jp.common.security.JwtSubject;
import com.example.web_jp.common.security.UserPrinciple;
import com.example.web_jp.service.CustomUserDetailsService;
import com.example.web_jp.util.JwtUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

//Tạo class JwtRequestFilter kế thừa class OncePerRequestFilter.
//Mỗi khi có request, lớp lọc này sẽ được thực thi.
//Nó nhiệm vụ kiểm tra trong request đến có chứa token JWT hợp lệ không?
//Nếu hợp lệ thì tạo object Authentication chứa thông tin principal hiện tại lưu vào SecurityContext
//→ Xác thực thành công.


public class AuthFilter extends OncePerRequestFilter {

    @Autowired
    private JwtProvider tokenProvider;

    @Value("${tokenType}")
    private String tokenType;

    @Value("${Authorization}")
    private String keyHeaderAuthor;

    private static final Logger LOGGER = LoggerFactory.getLogger(AuthFilter.class);

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response,
                                    FilterChain filterChain)
            throws ServletException, IOException {

        try {
            String token = getToken(request);
            if (token != null && tokenProvider.validateJwtToken(token, request)) {
                JwtSubject jwtSubject = tokenProvider.getSubjectFromJwtToken(token); // convert jwt string to jwt subject
                UserDetails userDetails = new UserPrinciple();
                /**
                 *  copy properties from jwtsubject to userDetail;
                 *  lưu ý các trường của 2 class giống tên nhau thì mới copy được
                 */
                BeanUtils.copyProperties(jwtSubject, userDetails);

                // put userDetail vào UsernamePasswordAuthenticationToken
                UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(
                        userDetails, null, userDetails.getAuthorities());
                authentication
                        .setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
                // Put UsernamePasswordAuthenticationToken vào SecurityContextHolder(Nơi khi check quyền các path ở Security config sẽ lấy role ở trong đây)
                SecurityContextHolder.getContext().setAuthentication(authentication);
            }
        } catch (Exception e) {
            LOGGER.error("Can NOT set user authentication -> Message: {}", e.getMessage());
            LOGGER.debug("Can NOT set user authentication -> error: ", e);
        }
        filterChain.doFilter(request, response);
    }

    /**
     *
     * @param request
     * @return extract chuỗi jwt từ header request
     */
    private String getToken(HttpServletRequest request) {
        String strTokenType = tokenType.concat(" ");
        String authHeader = request.getHeader(keyHeaderAuthor);
        if (authHeader != null && authHeader.startsWith(strTokenType)) {
            return authHeader.replace(strTokenType, "");
        }
        return null;
    }
}
