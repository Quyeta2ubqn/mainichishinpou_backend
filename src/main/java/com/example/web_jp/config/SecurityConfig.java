package com.example.web_jp.config;

import com.example.web_jp.common.constant.CommonConstant;
import com.example.web_jp.common.security.CloudCenterAuthenticationEntryPoint;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.configurers.AbstractHttpConfigurer;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    public CloudCenterAuthenticationEntryPoint authenticationHandler;

    @Bean
    public AuthFilter authenticationJwtTokenFilter() {
        return new AuthFilter();
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        /**
         * Khuyến nghị sử dụng bảo vệ CSRF cho bất kỳ yêu cầu nào có thể được xử lý bởi trình duyệt bởi người dùng bình thường.
         * Nếu chỉ đang tạo một dịch vụ được sử dụng bởi các máy khách không sử
         * dụng trình duyệt(Client - Server,Mobile-Frontend-Backend)
         * tắt tính năng bảo vệ CSRF.
         */
        http.csrf(AbstractHttpConfigurer::disable);
        /**
         * Tạo CommonConstant để hạn chế code phức tạp dễ maintain update
         * pathsNoAuth là các path không cần authentication
         */
        String[] pathsNoAuth = new String[CommonConstant.PATHS_NO_AUTHENTICATION.size()];
        CommonConstant.PATHS_NO_AUTHENTICATION.toArray(pathsNoAuth);
        System.out.println(pathsNoAuth);
        http.authorizeRequests()
                .antMatchers(pathsNoAuth).permitAll()
                .anyRequest().authenticated()
                .and()
                .exceptionHandling().authenticationEntryPoint(authenticationHandler)
                .and().sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);

        http.addFilterBefore(authenticationJwtTokenFilter(),
                UsernamePasswordAuthenticationFilter.class);
    }
}
