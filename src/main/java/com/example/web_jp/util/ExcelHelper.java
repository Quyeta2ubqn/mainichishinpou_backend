package com.example.web_jp.util;

import com.example.web_jp.pojo.entity.Nguphap_N45;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.web.multipart.MultipartFile;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ExcelHelper {
    public static String TYPE = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
    static String[] HEADERs = { "id", "name", "imi", "setsuzoku", "reibun" };
    static String SHEET = "Sheet1";

    public static boolean hasExcelFormat(MultipartFile file){
        if(!TYPE.equals(file.getContentType())){
            return false;
        }
        return true;
    }

    public static ByteArrayInputStream exportFile(List<Nguphap_N45> nguphap_n45s) throws IOException {
        try (Workbook workbook = new XSSFWorkbook(); ByteArrayOutputStream out = new ByteArrayOutputStream();) {
            Sheet sheet = workbook.createSheet(SHEET);
            // Header
            Row headerRow = sheet.createRow(0);

            for (int col = 0; col < HEADERs.length; col++) {
                Cell cell = headerRow.createCell(col);
                cell.setCellValue(HEADERs[col]);
            }

            int rowIdx = 1;
            for (Nguphap_N45 nguphapN45:nguphap_n45s){
                Row row = sheet.createRow(rowIdx++);
                row.createCell(0).setCellValue(nguphapN45.getId());
                row.createCell(1).setCellValue(nguphapN45.getName());
                row.createCell(2).setCellValue(nguphapN45.getImi());
                row.createCell(3).setCellValue(nguphapN45.getSetsuzoku());
                row.createCell(4).setCellValue(nguphapN45.getReibun());
            }

            workbook.write(out);
            return new ByteArrayInputStream(out.toByteArray());
        }catch (IOException ex){
            throw new RuntimeException("fail to export data to Excel file:" +ex.getMessage());
        }
    }

    public static List<Nguphap_N45> importFile(InputStream inputStream) throws IOException{
        try {
            Workbook workbook = new XSSFWorkbook(inputStream);
            Sheet sheet = workbook.getSheet(SHEET);
            Iterator<Row> rows = sheet.iterator();
            List<Nguphap_N45> nguphapN45s = new ArrayList<Nguphap_N45>();
            int rowNumber = 0;
            while (rows.hasNext()){
                Row currentRow = rows.next();
                //skip header
                if(rowNumber == 0){
                    rowNumber++;
                    continue;
                }
                Iterator<Cell> cellsInRow = currentRow.iterator();
                Nguphap_N45 nguphapN45 = new Nguphap_N45();
                int cellInx = 0;
                while (cellsInRow.hasNext()){
                    Cell currentCell = cellsInRow.next();
                    switch (cellInx){
                        case 0:
                            nguphapN45.setId((long) currentCell.getNumericCellValue());
                            break;
                        case 1:
                            nguphapN45.setName(currentCell.getStringCellValue());
                            break;
                        case 2:
                            nguphapN45.setImi(currentCell.getStringCellValue());
                            break;
                        case 3:
                            nguphapN45.setSetsuzoku(currentCell.getStringCellValue());
                            break;
                        case 4:
                            nguphapN45.setReibun(currentCell.getStringCellValue());
                            break;
                        default:
                            break;
                    }
                    cellInx++;
                }
                nguphapN45s.add(nguphapN45);
            }
            workbook.close();
            return nguphapN45s;
        } catch (IOException e) {
            throw new RuntimeException("fail to parse Excel file: " + e.getMessage());
        }
    }
}
