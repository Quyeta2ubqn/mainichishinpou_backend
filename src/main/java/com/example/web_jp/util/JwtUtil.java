package com.example.web_jp.util;

import io.jsonwebtoken.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;


@Service
public class JwtUtil {

    Logger logger = LoggerFactory.getLogger(JwtUtil.class);

    public static final String PREFIX = "Bearer ";
    public int duration = 86400;
    public String secret = "mainichishinpou";

    public String extractUsername(String token) {
        return extractClaim(token, Claims::getSubject);
    }

    public Date extractExpiration(String token) {
        return extractClaim(token, Claims::getExpiration);
    }

    public <T> T extractClaim(String token, Function<Claims, T> claimsResolver) {
        final Claims claims = extractAllClaims(token);
        return claimsResolver.apply(claims);
    }
    private Claims extractAllClaims(String token) {
        return Jwts.parser().setSigningKey(secret).parseClaimsJws(token).getBody();
    }

    private Boolean isTokenExpired(String token) {
        return extractExpiration(token).before(new Date());
    }

    public String generateToken(String username) {
        Map<String, Object> claims = new HashMap<>();
        return createToken(claims, username);
    }

    private String createToken(Map<String, Object> claims, String subject) {
        // 1. Định nghĩa các claims: issuer, expiration, subject, id
        // 2. Mã hóa token sử dụng thuật toán HS256 và key bí mật
        // 3. Convert thành chuỗi URL an toàn
        // 4. Cộng chuỗi đã sinh ra với tiền tố Bearer

        return PREFIX +  Jwts.builder().setClaims(claims).setSubject(subject).setIssuedAt(new Date(System.currentTimeMillis()))
                .setExpiration(new Date(System.currentTimeMillis() + 1000 * duration))
                .signWith(SignatureAlgorithm.HS256, secret).compact();
    }

    public Boolean validateToken(String token) throws RuntimeException {
        try {
            Jwts.parser().setSigningKey(secret).parseClaimsJws(token);
            return true;
        } catch (SignatureException e) {
            logger.error("Invalid JWT signature trace: {}", e.getMessage());
            throw new RuntimeException(e);
        } catch (MalformedJwtException e) {
            logger.error("Invalid JWT token trace: {}", e.getMessage());
            throw new RuntimeException(e);
        } catch (ExpiredJwtException e) {

            logger.error("Expired JWT token trace: {}", e);
            throw new RuntimeException(e);
        } catch (UnsupportedJwtException e) {

            logger.error("Unsupported JWT token trace: {}", e);
            throw new RuntimeException(e);
        } catch (IllegalArgumentException e) {

            logger.error("JWT token compact of handler are invalid trace: {}", e);
            throw new RuntimeException(e);
        }
    }
}
