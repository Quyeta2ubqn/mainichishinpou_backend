package com.example.web_jp.enums;

public enum ResponseEnum {

    /**
     * <p> 00000 Success </p>
     * <p> 1xxxx Error input data </p>
     * <p> 2xxxx Error business </p>
     * <p> 5xxxx Error Common service OTP </p>
     */
    SUCCESS("00000", "Successful"),

    //Error login
    LOGIN_FAIL("20000", "Login fail"),
    USERNAME_NOT_EXIST("20002", "Username does not exist"),
    LOGIN_INCORRECT_PASSWORD("20003", "Incorrect password"),
    LOGIN_INCORRECT_EXCEEDED("20004",
            "Your account has been temporarily suspended because you have exceeded the maximum number of incorrect attempts. Please contact LAMB hotline for assistance"),
    ACCOUNT_BLOCKED("20005",
            "Your account has been blocked. Please contact LAMB hotline for assistance"),
    ACCOUNT_TEMPORARILY_BLOCKED("20006",
            "Your account has been temporarily suspended. Please try again later "),
    ACCOUNT_USERNAME_EXISTED("20008", "Username already exists"),
    ACCOUNT_USERNAME_WRONG_FORMAT("20009", "Username wrong format"),
    ACCOUNT_ALREADY_USERNAME("20012", "Account already has username"),
    OTP_REQUEST_FAIL("20013", "Request OTP fail"),
    USERNAME_OR_PHONE_NOT_MATCH("20015", "Username does not exist or phone number does not match"),
    NEW_PASS_NOT_SAME_RECENT("20016",
            "The new password must not be the same as the current password or the most recent password"),
    OTP_VERIFY_FAIL("20001", "Verify OTP fail"),
    PASSWORD_INVALID("20017", "Password invalid"),
    RESET_PASSWORD_FAIL("20018", "Reset Password Fail"),
    OLD_PASSWORD_INCORRECT("20019", "Old password incorrect"),
    CHANGE_PASSWORD_FAIL("20020", "Change Password Fail"),
    ACCOUNT_IS_NOT_SUPPORT("20020", "Account is not support"),

    INPUT_VALID("99997", "Input invalid. Please try again"),
    NOT_AUTHORIZATION("99998", "Not authorization"),
    ERROR("99999", "The system is busy, please try again later");


    private final String code;

    private final String message;

    public String getCode() {
        return code;
    }


    public String getMessage() {
        return message;
    }

    ResponseEnum(String code, String message) {
        this.code = code;
        this.message = message;
    }

}
