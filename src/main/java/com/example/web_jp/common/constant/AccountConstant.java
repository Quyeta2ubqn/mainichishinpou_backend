package com.example.web_jp.common.constant;

public class AccountConstant {
    private AccountConstant() {

    }

    public static final int NOT_CONFIRM_STATUS = 0;
    public static final int ACTIVE_STATUS = 1;
    public static final int BLOCKED_STATUS = 3;
    public static final String DEFAULT_ROLE = "USER";


}
