package com.example.web_jp.common.constant;

import java.util.Arrays;
import java.util.List;

public class CommonConstant {
    private CommonConstant(){
    }

    public static final String SUCCESS = "Successful";

    public static final List<String> PATHS_NO_AUTHENTICATION = Arrays.asList("/authenticate",
            "/swagger/**",
            "/swagger-ui/**",
            "/swagger-ui.html", "/v3/api-docs/**", "/health",
            "/api/auth/register",
            "/api/auth/login",
            "/swagger-ui/**",
            "/swagger-ui**",
            "/v3/api-docs",
            "/configuration/ui",
            "/swagger-resources/**",
            "/configuration/**",
            "/swagger-ui.html",
            "/webjars/**"
    );
    public static final String HEADER_KEY_LANG = "lang";

    public static final String DATE_FORMAT_DD_MM_YYYY = "dd/MM/yyyy";

    public static final String DATE_FORMAT_YYYY_MM_DD = "yyyy-MM-dd";

    public static final String DATE_FORMAT_SIMPLE = "ddMMyyyy";

    public static final String DEFAULT_LANGUAGE = "vi";


    public static final String AUTH_ERROR_TYPE = "AuthErrorType";

    public static final String AUTH_ERROR_TOKEN_EXPIRE = "TokenExpire";

    public static final String AUTH_TOKEN_EXPIRE_MESSAGE = "Phiên đăng nhập đã hết hạn hoặc tài khoản đang đăng nhập trên 1 thiết bị khác";

    public static final String NOT_LOGIN_MESSAGE = "Bạn chưa đăng nhập";

    public static final long MAX_TIME_LOGIN = 900; // max time for one session login unit seconds

    public static final String X_OTP_SESSION_ID = "x-otp-token";
}
