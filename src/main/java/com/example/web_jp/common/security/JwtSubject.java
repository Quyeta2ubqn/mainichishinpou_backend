package com.example.web_jp.common.security;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Data
public class JwtSubject {

    private long id;

    private String firstname;

    private String lastname;

    private String username;

    private long age;

    private String phone;

    private String token;

    private String email;

    private String password;

    private String avatar;

    private int status;

    private boolean deleted;

    private String role;

}
