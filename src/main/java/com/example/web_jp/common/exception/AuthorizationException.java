package com.example.web_jp.common.exception;


import com.example.web_jp.enums.ResponseEnum;

public class AuthorizationException extends RuntimeException {

    public AuthorizationException() {
        super(ResponseEnum.NOT_AUTHORIZATION.getMessage());

    }

    public AuthorizationException(String message) {
        super(message);

    }

}
