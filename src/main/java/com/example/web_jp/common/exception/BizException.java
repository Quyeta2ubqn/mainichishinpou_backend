package com.example.web_jp.common.exception;


import com.example.web_jp.enums.ResponseEnum;

public class BizException extends RuntimeException {

    private String code;

    public BizException() {
    }

    public BizException(ResponseEnum responseEnum, String message) {
        super(message);
        this.code = responseEnum.getCode();
    }

    public BizException(ResponseEnum responseEnum) {
        super(responseEnum.getMessage());
        this.code = responseEnum.getCode();
    }

    public BizException(String code, String message) {
        super(message);
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

}
