package com.example.web_jp.pojo.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;

import java.util.Date;

import static com.example.web_jp.common.constant.AccountConstant.DEFAULT_ROLE;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "Account",schema = "public")
public class Account {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private long id;

    @Column(name = "firstname")
    private String firstname;

    @Column(name = "lastname")
    private String lastname;

    @Column(name = "username")
    private String username;

    @Column(name = "age")
    private long age;

    @Column(name="phone")
    private String phone;

    @Column(name = "token")
    private String token;

    @Column(name = "email")
    private String email;

    @Column(name="password")
    private String password;

    @Column(name="avatar")
    private String avatar;

    @Column(name = "status")
    private int status;

    @Column(name="deleted", columnDefinition = "boolean default false")
    private boolean deleted;

    @Column(name = "role", nullable = false, columnDefinition = "varchar(255) default 'USER'")
    private String role = DEFAULT_ROLE;

    private String updatedBy;

    private String createdBy;

    @CreationTimestamp
    @Column(updatable = false)
    private Date createdDate;

    @UpdateTimestamp
    private Date updatedDate;
}
