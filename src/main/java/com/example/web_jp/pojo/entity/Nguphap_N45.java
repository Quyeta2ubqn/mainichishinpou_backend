package com.example.web_jp.pojo.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "nguphap_n45")
public class Nguphap_N45 {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private long id;

    @Column(name = "name")
    private String name;

    @Column(name = "imi")
    private String imi;

    @Column(name = "setsuzoku")
    private String setsuzoku;

    @Column(name = "reibun")
    private String reibun;
}
