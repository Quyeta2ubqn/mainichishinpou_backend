package com.example.web_jp.pojo.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "kotoba_notify")
public class KotobaNotify {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "kotoba_jp")
    private String kotoba_jp;

    @Column(name = "kotoba_en")
    private String kotoba_en;

    @Column(name = "imi")
    private String imi;

}
