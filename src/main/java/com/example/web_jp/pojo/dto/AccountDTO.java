package com.example.web_jp.pojo.dto;


import lombok.Data;

@Data
public class AccountDTO {
    private long id;

    private String firstname;

    private String lastname;

    private String username;

    private long age;

    private String phone;

    private String token;

    private String email;

    private String password;

    private String avatar;

    private int status;

    private boolean deleted;

    private String role;
}
