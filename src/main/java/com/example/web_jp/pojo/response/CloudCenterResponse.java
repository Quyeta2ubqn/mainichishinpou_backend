package com.example.web_jp.pojo.response;

import com.example.web_jp.enums.ResponseEnum;
import lombok.Data;

import java.util.Date;

@Data
public class CloudCenterResponse<T>{

    private String responseCode;

    private String message;

    private Date timestamp = new Date();

    private T data;

    private static <T> CloudCenterResponse<T> build() {
        return new CloudCenterResponse<>();
    }

    public static <T> CloudCenterResponse<T> ok(T data) {
        CloudCenterResponse<T> response = build();
        response.setResponseCode(ResponseEnum.SUCCESS.getCode());
        response.setMessage(ResponseEnum.SUCCESS.getMessage());
        response.setData(data);
        return response;
    }

    public static <T> CloudCenterResponse<T> ok(T data, String message) {
        CloudCenterResponse<T> response = ok(data);
        response.setMessage(message);
        return response;
    }
}
