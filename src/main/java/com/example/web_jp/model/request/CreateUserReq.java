package com.example.web_jp.model.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class CreateUserReq {

    private String firstname;

    private String lastname;

    private String username;

    private String email;

    private String password;

    private String phone;

    private int status;
}
