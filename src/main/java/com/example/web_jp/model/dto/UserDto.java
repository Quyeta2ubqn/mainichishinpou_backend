package com.example.web_jp.model.dto;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UserDto {
    private long id;

    private String firstname;

    private String lastname;

    private String username;

    private String email;

    private String phone;


}
