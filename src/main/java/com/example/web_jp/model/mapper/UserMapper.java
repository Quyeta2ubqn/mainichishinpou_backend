package com.example.web_jp.model.mapper;

import com.example.web_jp.pojo.entity.Account;
import com.example.web_jp.model.dto.UserDto;
import com.example.web_jp.model.request.CreateUserReq;
import com.example.web_jp.model.request.UpdateUserReq;
import org.springframework.security.crypto.bcrypt.BCrypt;

import java.time.Instant;

public class UserMapper {
    private static final int UNACTIVE = 1;

    public static UserDto toUserDto(Account account){
        UserDto tmp = new UserDto();
        tmp.setId(account.getId());
        tmp.setFirstname(account.getFirstname());
        tmp.setLastname(account.getLastname());
        tmp.setEmail(account.getEmail());
        tmp.setPhone(account.getPhone());
        tmp.setUsername(account.getUsername());
        return tmp;
    }

    public static Account toUser(CreateUserReq req){
        Account account = new Account();
        Instant instant = Instant.now();
        long time = instant.toEpochMilli();
        account.setFirstname(req.getFirstname());
        account.setLastname(req.getLastname());
        String hash = BCrypt.hashpw(req.getPassword(), BCrypt.gensalt(12));
        account.setPassword(hash);
        account.setEmail(req.getEmail());
        account.setRole("USER");
        account.setPhone(req.getPhone());
        account.setStatus(UNACTIVE);
//        account.setC(time);
//        account.setUpdate_at(time);
        account.setUsername(req.getUsername());

        return account;
    }

    public static Account updateUser(Account account, UpdateUserReq req){
        Instant instant = Instant.now();
        long time = instant.toEpochMilli();

        account.setFirstname(req.getFirstname());

        account.setLastname(req.getLastname());

        account.setEmail(req.getEmail());

        account.setPhone(req.getPhone());

//        account.setUpdate_at(time);

        account.setUsername(req.getUsername());

        return account;
    }
}
