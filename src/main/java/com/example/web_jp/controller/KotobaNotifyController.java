package com.example.web_jp.controller;

import com.example.web_jp.pojo.entity.KotobaNotify;
import com.example.web_jp.service.kotobaNotify.KotobaNotifyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class KotobaNotifyController {

    @Autowired
    KotobaNotifyService kotobaNotifyService;

    @GetMapping("/kotobaNotifys")
    public List<KotobaNotify> getAllUser(){
        return kotobaNotifyService.getAll();
    }
}
