package com.example.web_jp.controller;

import com.example.web_jp.pojo.entity.Nguphap_N45;
import com.example.web_jp.model.ResponseMessage;
import com.example.web_jp.service.excel.ExcelService;
import com.example.web_jp.util.ExcelHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

@RestController
public class ExcelController {

    @Autowired
    private ExcelService excelService;

    @PostMapping("/upload")
    public ResponseEntity<ResponseMessage> importFile(@RequestParam("file")MultipartFile file){
        String msg = "";
        if(ExcelHelper.hasExcelFormat(file)){
            try{
                excelService.save(file);
                msg = "import file success: " + file.getOriginalFilename();
                return ResponseEntity.status(HttpStatus.OK).body(new ResponseMessage(msg));
            }catch (Exception e){
                msg = "could not import file: " + file.getOriginalFilename();
                return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(new ResponseMessage(msg));
            }
        }
        msg = "please import file";
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ResponseMessage(msg));
    }

    @GetMapping("/nguphapn45")
    public ResponseEntity<List<Nguphap_N45>> getAllNguphapN45(){
        try{
            List<Nguphap_N45> nguphapN45s = excelService.getAllNguphap_N45();
            if(nguphapN45s.isEmpty()){
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
            return new ResponseEntity<>(nguphapN45s, HttpStatus.OK);
        }catch (Exception e){
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/download")
    public ResponseEntity<Resource> getFile() throws IOException {
        String filename = "NguPhapN45";
        InputStreamResource file = new InputStreamResource(excelService.load());

        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + filename)
                .contentType(MediaType.parseMediaType("application/vnd.ms-excel"))
                .body(file);
    }
}
