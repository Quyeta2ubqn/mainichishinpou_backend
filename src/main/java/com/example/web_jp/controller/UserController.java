package com.example.web_jp.controller;

import com.example.web_jp.config.AuthFilter;
import com.example.web_jp.model.dto.UserDto;
import com.example.web_jp.model.request.AuthRequest;
import com.example.web_jp.model.request.CreateUserReq;
import com.example.web_jp.model.request.UpdateUserReq;
import com.example.web_jp.pojo.entity.Account;
import com.example.web_jp.service.User.UserService;
import com.example.web_jp.util.JwtUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
public class UserController {

    @Autowired
    UserService userService;

    @Autowired
    private JwtUtil jwtUtil;

    @Autowired
    private AuthFilter jwtFilter;


    @Autowired
    private JavaMailSender mailSender;

    @GetMapping("/users")
    public List<Account> getAllUser() {
        return userService.getAll();
    }

    @GetMapping("/")
    public ResponseEntity<?> welcome() {

        return ResponseEntity.ok("hello world");
    }

    @PostMapping("/authenticate")
    public String generateToken(@RequestBody AuthRequest authRequest) throws Exception {
        // Gen token
        return jwtUtil.generateToken(authRequest.getUsername());
    }

    @PostMapping("/create-user")
    public ResponseEntity<?> createUser(@Valid @RequestBody CreateUserReq req) {
        UserDto rs = userService.createUser(req);
        return ResponseEntity.ok(rs);
    }

    @PutMapping("/user/edit/{id}")
    public ResponseEntity<?> editUser(@PathVariable("id") long id, @Valid @RequestBody UpdateUserReq req) {
        UserDto rs = userService.editUser(id, req);
        return ResponseEntity.ok(rs);
    }

    @DeleteMapping("/user/delete/{id}")
    public ResponseEntity deleteUser(@PathVariable("id") long id) {
        userService.deletebyId(id);
        return ResponseEntity.ok("Delete success");
    }

    @GetMapping("/send-simple-email")
    public String sendSimpleEmail() {
        // Create a Simple MailMessage.
        SimpleMailMessage message = new SimpleMailMessage();

        message.setTo("quyetvv12795@gmail.com");
        message.setSubject("Test Simple Email");
        message.setText("Hello World");

        // Send Message!
        mailSender.send(message);
        return "Email sent";
    }
}
