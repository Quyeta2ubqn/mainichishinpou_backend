package com.example.web_jp.controller;

import com.example.web_jp.common.constant.CommonConstant;
import com.example.web_jp.pojo.request.LoginRequest;
import com.example.web_jp.pojo.request.RegisterAccountRequest;
import com.example.web_jp.pojo.response.CloudCenterResponse;
import com.example.web_jp.pojo.response.LoginResponse;
import com.example.web_jp.service.account.AccountService;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/auth")
public class AuthController {

    @Autowired
    AccountService accountService;

    @PostMapping("/register")
    public CloudCenterResponse<String> register(@Valid @RequestBody RegisterAccountRequest body) {
        accountService.reg(body);
        return CloudCenterResponse.ok(CommonConstant.SUCCESS,"Register Success !");
    }

    @PostMapping("/login")
    public CloudCenterResponse<LoginResponse> login(@Valid @RequestBody LoginRequest body) throws JsonProcessingException {
        return CloudCenterResponse.ok(accountService.login(body),"Login Success !");
    }
}
