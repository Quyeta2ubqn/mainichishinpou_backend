package com.example.web_jp.service;

import com.example.web_jp.pojo.entity.Account;
import com.example.web_jp.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public class CustomUserDetailsService implements UserDetailsService {

    @Autowired
    private UserRepository userRepository;
    @Override
    public UserDetails loadUserByUsername(String firstname) throws UsernameNotFoundException {
        Account account = userRepository.findByFirstname(firstname);
        return new org.springframework.security.core.userdetails.User(account.getFirstname(), account.getPassword(), new ArrayList<>());
    }
}
