package com.example.web_jp.service.User;

import com.example.web_jp.pojo.entity.Account;
import com.example.web_jp.exception.DuplicateRecordException;
import com.example.web_jp.model.dto.UserDto;
import com.example.web_jp.model.mapper.UserMapper;
import com.example.web_jp.model.request.CreateUserReq;
import com.example.web_jp.model.request.UpdateUserReq;
import com.example.web_jp.repository.UserRepository;
import com.example.web_jp.util.JwtUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class UserServiceImpl implements UserService {

    @Autowired
    UserRepository userRepository;

    @Autowired
    JwtUtil jwtUtil;
    @Override
    public List<Account> getAll() {
        return userRepository.findAll();
    }

    @Override
    public UserDto createUser(CreateUserReq req) {
        // Check email exist
        Account account = userRepository.findByEmail(req.getEmail());
        if(account != null){
            throw new DuplicateRecordException("Email is already in use");
        }
        account = UserMapper.toUser(req);
        userRepository.save(account);
        return UserMapper.toUserDto(account);
    }

    @Override
    public UserDto editUser(long id, UpdateUserReq req) {
        Account account = userRepository.findById(id).get();

        if(account != null){

            account = UserMapper.updateUser(account, req);
            userRepository.save(account);
        }else{
            throw new DuplicateRecordException("User not found by id");
        }
        return UserMapper.toUserDto(account);
    }

    @Override
    public void deletebyId(long id) {
        userRepository.deleteById(id);
    }

}
