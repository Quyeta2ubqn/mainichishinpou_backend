package com.example.web_jp.service.User;

import com.example.web_jp.pojo.entity.Account;
import com.example.web_jp.model.dto.UserDto;
import com.example.web_jp.model.request.CreateUserReq;
import com.example.web_jp.model.request.UpdateUserReq;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface UserService {

    public List<Account> getAll();
    public UserDto createUser(CreateUserReq req);

    UserDto editUser(long id, UpdateUserReq req);

    void deletebyId(long id);
}
