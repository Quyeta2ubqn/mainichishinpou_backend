package com.example.web_jp.service.account;

import com.example.web_jp.common.constant.AccountConstant;
import com.example.web_jp.common.exception.BizException;
import com.example.web_jp.common.security.JwtProvider;
import com.example.web_jp.common.security.JwtSubject;
import com.example.web_jp.enums.ResponseEnum;
import com.example.web_jp.pojo.dto.AccountDTO;
import com.example.web_jp.pojo.entity.Account;
import com.example.web_jp.pojo.request.LoginRequest;
import com.example.web_jp.pojo.request.RegisterAccountRequest;
import com.example.web_jp.pojo.response.LoginResponse;
import com.example.web_jp.repository.AccountRepository;
import com.google.gson.Gson;
import org.modelmapper.ModelMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class AccountServiceImpl implements  AccountService{

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private JwtProvider jwtProvider;

    @Autowired
    private Gson gson;

    @Autowired
    private ModelMapper mapper;

    @Override
    public AccountDTO reg(RegisterAccountRequest request) {
        Account accountExist = accountRepository.findByEmail(request.getEmail());
        if (accountExist != null) {
            throw new BizException(ResponseEnum.ACCOUNT_USERNAME_EXISTED, "Email đã được sử dụng");
        }
        Account account = new Account();
        BeanUtils.copyProperties(request, account);
        if (!request.getPassword().equals(request.getReEnterPassword())) {
            throw new BizException(ResponseEnum.PASSWORD_INVALID, "Password not same ReEnterPassword");
        }
        account.setPassword(passwordEncoder.encode(request.getPassword()));
        return mapper.map(accountRepository.save(account),AccountDTO.class);
    }

    @Override
    public LoginResponse login(LoginRequest request) {
        Account account = accountRepository.findAccountByEmailOrUsernameOrPhone(request.getUsername(), request.getUsername(), request.getUsername());
        if (account == null) {
            throw new BizException(ResponseEnum.USERNAME_NOT_EXIST, "Username does not exist");
        }
        if (!passwordEncoder.matches(request.getPassword(), account.getPassword())) {
            throw new BizException(ResponseEnum.LOGIN_INCORRECT_PASSWORD, "Username or password incorrect");
        }
        if (account.getStatus() == AccountConstant.BLOCKED_STATUS) {
            throw new BizException(ResponseEnum.ACCOUNT_BLOCKED, "Your account has been blocked. Please contact CloudCenter hotline for assistance");
        }
        LoginResponse response = new LoginResponse();
        JwtSubject jwtSubject = new JwtSubject();
        BeanUtils.copyProperties(account, jwtSubject);
        BeanUtils.copyProperties(account, response);
        String accessToken = jwtProvider.generateJwtToken(jwtSubject);
        response.setAccessToken(accessToken);
        response.setTokenType("Bearer");
        return response;
    }

}
