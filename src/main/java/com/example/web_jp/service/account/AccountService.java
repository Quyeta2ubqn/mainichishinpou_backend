package com.example.web_jp.service.account;

import com.example.web_jp.pojo.dto.AccountDTO;
import com.example.web_jp.pojo.request.LoginRequest;
import com.example.web_jp.pojo.request.RegisterAccountRequest;
import com.example.web_jp.pojo.response.LoginResponse;

public interface AccountService {
    public AccountDTO reg(RegisterAccountRequest request);

    public LoginResponse login(LoginRequest request);
}
