package com.example.web_jp.service.kotobaNotify;

import com.example.web_jp.pojo.entity.KotobaNotify;
import com.example.web_jp.repository.KotobaNotifyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class KotobaNotifyServiceImpl implements KotobaNotifyService {

    @Autowired
    private KotobaNotifyRepository kotobaNotifyRepository;

    @Override
    public List<KotobaNotify> getAll() {
        return kotobaNotifyRepository.findAll();
    }
}
