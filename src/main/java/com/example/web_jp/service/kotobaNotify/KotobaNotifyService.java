package com.example.web_jp.service.kotobaNotify;

import com.example.web_jp.pojo.entity.KotobaNotify;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface KotobaNotifyService {

    public List<KotobaNotify> getAll();
}
