package com.example.web_jp.service.excel;

import com.example.web_jp.pojo.entity.Nguphap_N45;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.List;

@Service
public interface ExcelService {
    public void save(MultipartFile file);

    public ByteArrayInputStream load() throws IOException;

    public List<Nguphap_N45> getAllNguphap_N45();
}
