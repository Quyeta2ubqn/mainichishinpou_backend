package com.example.web_jp.service.excel;

import com.example.web_jp.pojo.entity.Nguphap_N45;
import com.example.web_jp.repository.Nguphap_N45Repository;
import com.example.web_jp.util.ExcelHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.List;

@Component
public class ExcelServiceImpl implements ExcelService {

    @Autowired
    private Nguphap_N45Repository nguphap_n45Repository;

    @Override
    public void save(MultipartFile file) {
            try{
                List<Nguphap_N45> nguphapN45s = ExcelHelper.importFile(file.getInputStream());
                nguphap_n45Repository.saveAll(nguphapN45s);
            }catch (IOException e){
                throw new RuntimeException("fail to store excel data" + e.getMessage());
            }
    }

    @Override
    public ByteArrayInputStream load() throws IOException {
        List<Nguphap_N45> nguphapN45s = nguphap_n45Repository.findAll();
        ByteArrayInputStream inputStream = ExcelHelper.exportFile(nguphapN45s);
        return inputStream;
    }

    @Override
    public List<Nguphap_N45> getAllNguphap_N45() {
        return nguphap_n45Repository.findAll();
    }
}
