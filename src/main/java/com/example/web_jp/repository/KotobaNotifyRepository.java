package com.example.web_jp.repository;

import com.example.web_jp.pojo.entity.KotobaNotify;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface KotobaNotifyRepository extends JpaRepository<KotobaNotify, Long> {
}
