package com.example.web_jp.repository;

import com.example.web_jp.pojo.entity.Account;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface UserRepository extends JpaRepository<Account, Long > {

    Account findByFirstname(String username);

    public Account findByEmail(String email);

}
