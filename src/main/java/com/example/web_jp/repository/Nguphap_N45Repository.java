package com.example.web_jp.repository;

import com.example.web_jp.pojo.entity.Nguphap_N45;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface Nguphap_N45Repository extends JpaRepository<Nguphap_N45, Long> {
}
