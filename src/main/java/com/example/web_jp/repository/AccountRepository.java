package com.example.web_jp.repository;

import com.example.web_jp.pojo.entity.Account;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AccountRepository extends JpaRepository<Account,Long> {

    public Account findByEmail(String email);

    Account findAccountByEmailOrUsernameOrPhone(String email,String username, String phone);
}
