package com.example.web_jp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@SpringBootApplication
@CrossOrigin(origins = "*")
public class WebJpApplication {

	@Bean
	public WebMvcConfigurer corsConfigurer(){
		return new WebMvcConfigurer() {
			@Override
			public void addCorsMappings(CorsRegistry registry){
				registry.addMapping("/**")// Cho phép tất cả các đường dẫn
						.allowedOrigins("http://localhost:4200")
						.allowedMethods("GET", "POST", "PUT", "DELETE", "OPTIONS")
						.maxAge(3600L)
						.allowedHeaders("*")
						.exposedHeaders("authorization", "content-type", "x-auth-token")
						.allowCredentials(false);
			}
		};
	}

	public static void main(String[] args) {
		SpringApplication.run(WebJpApplication.class, args);
	}

}
